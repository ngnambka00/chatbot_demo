from distutils.sysconfig import customize_compiler
from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

class ActionGreetWithName(Action):
    def name(self) -> Text:
        return "action_greet_with_name"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        cust_sex = next(tracker.get_latest_entity_values("cust_sex"), None)
        cust_name = next(tracker.get_latest_entity_values("cust_name"), None)
        
        if cust_name is not None and cust_sex is not None: 
            dispatcher.utter_message(response = "utter_greet_with_name_sex", cust_sex=cust_sex.lower(), cust_name=cust_name)
        elif cust_name is not None and cust_sex is None: 
            dispatcher.utter_message(response = "utter_greet_with_name", cust_name =cust_name)
        elif cust_sex is not None and cust_name is None: 
            dispatcher.utter_message(response = "utter_greet_with_sex", cust_sex=cust_sex.lower())
        else: 
            dispatcher.utter_message(response="utter_greet_default")
        return []
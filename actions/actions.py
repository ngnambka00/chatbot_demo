from distutils.sysconfig import customize_compiler
from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
import feedparser


class ActionGetLottery(Action):

    def name(self) -> Text:
        return "action_get_lottery"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        url = 'https://xskt.com.vn/rss-feed/mien-bac-xsmb.rss'

        # Tien hanh lay thong tin tu URL
        feed_cnt = feedparser.parse(url)

        # Lay ket qua so xo moi nhat
        first_node = feed_cnt['entries']

        # Lay thong tin ve ngay va chi tiet cac giai
        return_msg = first_node[0]['title'] + "\n" + first_node[0]['description']

        dispatcher.utter_message(text=return_msg)

        return []